sap.ui.define([
		"sap/ui/core/UIComponent",
		"sap/ui/Device",
		"BubbleCount/model/models",
		"BubbleCount/controller/ErrorHandler"
		// ,"BubbleCount/controller/InspPointDialog"
	], function (UIComponent, Device, models, ErrorHandler) {
		"use strict";
		//https://activate-hr.de/sap-hr-entwicklung/errorhandling-in-sapui5-fiori-apps/
		return UIComponent.extend("BubbleCount.Component", {

			metadata : {
				manifest: "json"
			},
			
			/**
			 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
			 * In this function, the FLP and device models are set and the router is initialized.
			 * @public
			 * @override
			 */
			init : function () {
				// call the base component's init function
				UIComponent.prototype.init.apply(this, arguments);
				
				//Shortcut for logging
				//Deprecated as of version 1.58. use module:sap/base/Log instead
				var Log = jQuery.sap.log;
				Log.info( "sapui5 version = " + sap.ui.version);
				
				var oMessageManager  = sap.ui.getCore().getMessageManager();
				
				// initialize the error handler with the component
				this._oErrorHandler = new ErrorHandler(this);
				// set the device model
				this.setModel(models.createDeviceModel(), "device");
				// set the FLP model
				this.setModel(models.createFLPModel(), "FLP");
				// set the MII model
				
				
				
				
				// this._oMIIModel = this.getModel("MII");
				// this._oMIIModel.setErrorHandler(this._oErrorHandler);
				// this._oMIIModel.setAppModel(this.getModel("app"));
				// this._oMIIModel.setSizeLimit(Number.MAX_VALUE);
				// this._oMIIModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
				// oMessageManager.registerMessageProcessor(this._oMIIModel);
				
				
				// set the message model
				this.setModel(oMessageManager.getMessageModel(),"message");

				// create the views based on the url/hash
				this.getRouter().initialize();
				
				//https://www.clouddna.at/knowledge-base/sapui5-fragment-in-dialog/
				// set dialog
				// this._oInspPointDialog = new InspPointDialog();
				//this.inspPointDialog = new vpulltime.controller.InspPointDialog();
			},
			getErrorHandler: function() {
				return this._oErrorHandler;
			},
			// getMIIModel: function() {
			// 	return this._oMIIModel;
			// },
			// getInspPointDialog: function(){
			// 	return this._oInspPointDialog;
			// },
			/**
			 * The component is destroyed by UI5 automatically.
			 * In this method, the ErrorHandler is destroyed.
			 * @public
			 * @override
			 */
			destroy : function () {
				// this._oMIIModel.destroy();
				this._oErrorHandler.destroy();
				// this._oInspPointDialog.destroy();
				// call the base component's destroy function
				UIComponent.prototype.destroy.apply(this, arguments);
			},

			/**
			 * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
			 * design mode class should be set, which influences the size appearance of some controls.
			 * @public
			 * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
			 */
			getContentDensityClass : function() {
				if (this._sContentDensityClass === undefined) {
					// check whether FLP has already set the content density class; do nothing in this case
					if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
						this._sContentDensityClass = "";
					} else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
						this._sContentDensityClass = "sapUiSizeCompact";
					} else {
						// "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
						this._sContentDensityClass = "sapUiSizeCozy";
					}
				}
				return this._sContentDensityClass;
			}

		});

	}
);