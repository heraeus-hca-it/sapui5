sap.ui.define([
		"sap/ui/core/format/DateFormat"
	] , function (DateFormat) {
		"use strict";

		return {
			/**
			 * Concate date and description for the message list
			 * @public
			 * @param {long} iDate
			 * @param {string} sDescription 
			 * @returns {string} Date and Description or null
			 */			
			messageDetails: function(iDate, sDescription) {
				if (iDate && sDescription) {
					var oDateFormat = DateFormat.getDateTimeInstance({
						style: "long"
					});
					var sDateFormatted = oDateFormat.format(new Date(iDate));
					return sDateFormatted + "\r\n" + sDescription;
				} else {
					return null;
				}
			},
			dataProperty: function(oContext, sPath) {
				//var sValue = sPath;
				//match not support groups / matchAll does but no IE support
				var rExpr = new RegExp("\\{.*?\\}", "g");
				//var aParams = [];
				sPath.match(rExpr).forEach(function(value, index, array){
					//ignore repetitions
					if (index === array.indexOf(value)){
						var sParam = value.slice(1,value.length-1);
						var sValue = oContext.getProperty(sParam);
						//var sValue = $(oContext).find(sParam).text();
						//aParams.push({"param": sParam, "value": oVal});
						sPath = sPath.replace(new RegExp("\\{" + sParam + "\\}", "g"), sValue);
					}
				});
				return sPath;
			},
			stringFormat: function format(sPath, aParams) {
				debugger;
			    $.each(aParams,function (i, n) {
			        sPath = sPath.replace(new RegExp("\\{" + i + "\\}", "g"), n);
			    });
			    return sPath;
			},	
			messageCount : function (aMessages) {
				return aMessages.length || 0;
			},
			messageVisible : function (aMessages) {
				return aMessages && aMessages.length > 0;
			},
			/**
			 * Rounds the number unit value to 2 digits
			 * @public
			 * @param {string} sValue the number string to be rounded
			 * @returns {string} sValue with 2 digits rounded
			 */
			numberUnit : function (sValue) {
				if (!sValue) {
					return "";
				}
				return parseFloat(sValue).toFixed(2);
			},
			/**
			 * Format OData DateTime value
			 * @public
			 * @param {string} sValue JSON date string "/Date(##########)/"
			 * @param {string} sFormat the format string to be rounded
			 * @returns {string} sValue with 2 digits rounded
			 */
			odatadateformatter: function(sValue, sFormat) {
				if(!sFormat) {
					sFormat = "yy-mm-dd hh:MM:ss";
				}
				return sValue;
				//return dateFormat(dateTimeReviver(sValue), sFormat);
               //return dateFormat(new Date(sValue), format);
               

			},
			// jsonDateX: function(oValue, sFormat){
				
   //             if (typeof oValue === 'string') {
   //             	oValue  = eval(oValue.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
   //             }
   //             debugger;
			// 	if(!sFormat) {
			// 		sFormat = "yy-mm-dd hh:MM:ss";
			// 	}                
   //             return DateFormat.getDateInstance({
   //                 pattern: sFormat
   //             }).format(oValue);
                
			// },
			//oValue.toLocaleString('de-DE', { timeZone: 'Europe/Berlin' }) + " " + oValue.toLocaleTimeString('de-DE', { timeZone: 'Europe/Berlin', timeZoneName:'short' })
			// jsonDateY: function(oValue, sFormat){
			// 	var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
   //             if (typeof oValue === 'string') {
   //             	oValue  = eval(oValue.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
   //             }
			// 	if(!sFormat) {
			// 		sFormat = "yyyy-MM-dd HH:mm:ss v";
			// 	}
			// 	var sTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
			// 	var sTimeZoneShort = "";
				
			// 	//Fallback for IE11 is empty time zone
			// 	if (sTimeZone) {
			// 		//sTimeZone = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.timeZone");
			// 		sTimeZoneShort = oValue.toLocaleTimeString(sCurrentLocale, { timeZone: sTimeZone, timeZoneName:"short" }).split(" ")[1];
			// 	}
				
   //             var sDate = DateFormat.getDateInstance({
   //                 pattern: sFormat
   //             }).format(oValue);
			// 	if (sTimeZoneShort){
   //             	sDate = sDate.replace("v", sTimeZoneShort);
			// 	} else {
			// 		sDate = sDate.replace("v", "");
			// 	}
   //             return sDate;
			// },
			// jsonDateObject: function(oValue){
			// 	debugger;
   //             if (typeof oValue === 'string') {
   //             	oValue  = eval(oValue.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
   //             }
   //             return oValue;
			// },
			jsonDate: function(oValue, sFormat){
				
				
				//Intl.DateTimeFormat().resolvedOptions().locale 
				
				//var i18nModel = new sap.ui.model.resource.ResourceModel({bundleName:"vpulltime.i18n.i18n",bundleLocale:sCurrentLocale});
				//var test = i18nModel.getResourceBundle().getText("global.dlsTimeZoneShort")
				
                if (typeof oValue === 'string') {
                	oValue  = eval(oValue.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                }
				if(!sFormat) {
					return oValue;
					//sFormat = "yyyy-MM-dd HH:mm:ss v";
				}
				var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
				//var sTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
				var sTimeZoneShort = "";
				
				//Fallback for IE11 is empty time zone
				//if (sTimeZone) {
					//sTimeZone = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.timeZone");
					//sTimeZoneShort = oValue.toLocaleTimeString(sCurrentLocale, { timeZone: sTimeZone, timeZoneName:"short" }).split(" ")[1];
				//}

				var dJan1 = new Date(oValue.getFullYear(), 0, 1);
				var dJul1 = new Date(oValue.getFullYear(), 6, 1);
                var dlsTimeOffset = Math.max(dJan1.getTimezoneOffset(), dJul1.getTimezoneOffset());
                if (dlsTimeOffset !== 0 && oValue.getTimezoneOffset() === dJul1.getTimezoneOffset()){
					sTimeZoneShort = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.dlsTimeZoneShort");
                	sFormat = sFormat.replace("v", "'" + sTimeZoneShort + "'");
                } else {
					sTimeZoneShort = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.stdTimeZoneShort");
					sFormat = sFormat.replace("v", "'" + sTimeZoneShort + "'");
                }
				
				//Format date before timezone
                var sDate = DateFormat.getDateInstance({
                    pattern: sFormat
                }).format(oValue);
                
/*				var dJan1 = new Date(oValue.getFullYear(), 0, 1);
				var dJul1 = new Date(oValue.getFullYear(), 6, 1);
                var dlsTimeOffset = Math.max(dJan1.getTimezoneOffset(), dJul1.getTimezoneOffset());
                if (dlsTimeOffset !== 0 && oValue.getTimezoneOffset() === dJul1.getTimezoneOffset()){
					sTimeZoneShort = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.dlsTimeZoneShort");
                	sDate = sDate.replace("v", sTimeZoneShort);                	
                } else {
					sTimeZoneShort = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.stdTimeZoneShort");
					sDate = sDate.replace("v", sTimeZoneShort);
                }*/
                return sDate;
			},
			getHoursFromDate: function(dValue){
				return 5;
			}
		};
		
	}
);