sap.ui.define([
	"BubbleCount/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/format/DateFormat"
], function(BaseController, JSONModel, DateFormat) {
	"use strict";

	return BaseController.extend("BubbleCount.controller.Acquisition", {
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf BubbleCount.view.Acquisition
		 */
		onInit: function() {
			
			var oViewModel;
			oViewModel = new JSONModel({});
			
			this.setViewModel(oViewModel, "acquisition");
			//we need early data for view binding
			this.initViewData();
			
			this.getRouter().getRoute("acquisition").attachPatternMatched(this._onRoutePatternMatched, this);
		},		
		
		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private IlluminatorOData>/Rowset(QueryTemplate=\'MyContent_f23440%2FMF%2FVPullTime%2FGetAggGeometryData\',RowsetId=\'1\')/Row
		 */
		_onRoutePatternMatched: function(oEvent) {
			//Store the original route parameters 
			this._routeParameters = oEvent.getParameters();
			
			var aPromises = [
				this.getCompModel().metadataLoaded(), 
				this.getCompModel("app").loaded()
			];
			Promise.all(aPromises).then( function() {
				var oRouteArgs = this.getRouteParameters().arguments;
				if (oRouteArgs.ID) {
                    this.loadViewData();
                    
				} else {
					this.initViewData();
				    this.updateMachineSelect();
				}				
				
				
				// var sObjectPath = this.getModel().createKey("Objects", {
				// 	ObjectID :  sObjectId
				// });
				// this._bindView("/" + sObjectPath);
			}.bind(this));			
			
			
		},
		getSelectedMachineObject: function(){
			var oMachineSelect = this.getView().byId("machineSelect");
			var oItemsBindInfo = oMachineSelect.getBindingInfo("items");
			var oSelectedItem = oMachineSelect.getSelectedItem();
			return oSelectedItem.getBindingContext(oItemsBindInfo.model).getObject();
		},	
		updateMachineSelect: function(){
			var oRouteArgs = this.getRouteParameters().arguments;
			var oMachineSelect = this.getView().byId("machineSelect");
			var oItemsBindInfo = oMachineSelect.getBindingInfo("items");
			debugger;
			var oViewModelData = this.getView().getModel("acquisition").getData();
			var oSelectedItem;
			if(oRouteArgs.ID){
				oSelectedItem = oMachineSelect.getItems().filter(function(item){
					var obj = item.getBindingContext(oItemsBindInfo.model).getObject();
					return obj.value === oViewModelData.Machine;
				},this)[0];
				oMachineSelect.setSelectedItem(oSelectedItem);
			} else if (oRouteArgs.Workcenter && oRouteArgs.Plant){
				oSelectedItem = oMachineSelect.getItems().filter(function(item){
					var obj = item.getBindingContext(oItemsBindInfo.model).getObject();
					return obj.workcenter === oRouteArgs.Workcenter && obj.plant === oRouteArgs.Plant;
				},this)[0];
				if (oSelectedItem) {
					oMachineSelect.setSelectedItem(oSelectedItem);
					var oMachineData = oSelectedItem.getBindingContext(oItemsBindInfo.model).getObject();
					oViewModelData.Machine = oMachineData.value;				
				}				
			}

		},
		initViewData: function(){
			//var oRouteArgs = this.getRouteParameters().arguments;
			var oModel = this.getViewModel("acquisition");
			
			//var dToday = new Date(Date.parse("31 Oct 2021 00:00:00 GMT"));
			var dInspDate = new Date();
			dInspDate.setHours(dInspDate.getHours(),0,0,0 );
			
			//oAppModel.setProperty("/cutoff/date", dToday);			
			
			oModel.setData({
				"RowId": null,
				"ID": null,
				"Machine": "",
				"ProductionRun": "",
				"ChangedDatetimeUTC": null,
				"CreatedDatetimeUTC": null,
				"InspectionDatetimeUTC": dInspDate,
				"Particles": 0,
				"Bubbles2-4mm": 0,
				"Bubbles4-8mm": 0,
				"Bubbles8-12mm": 0,
				"BubblesGT12mm": 0,
				"BubblesLT2mm": 0,
				"SurfaceBubblesGT30mm": 0,
				"SurfaceBubblesLT30mm": 0,
				"Remark": "x",
				"Inspector": ""
			});
			
		},
		saveViewData: function(){
			var oModel = this.getCompModel();
			var oViewData = this.getView().getModel("acquisition").getData();
			var that = this;
			var fnSuccess = function(oData){
				
				
				oData.results[0].FatalError
				
				//var oRow = oData.results[0].Rowset.results[0].Row.results[0];
				//that.getView().getModel("acquisition").setData(oRow);
				debugger;
				var sMessage = oData.results[0].Messages.results[0].Message;
				sap.m.MessageToast.show(sMessage);
			};
			var fnError = function(oEvent){
				debugger;
			};
			debugger;
			var mParameters = {
			    headers: {},
				urlParameters: {
					"QueryTemplate": "HQS-HCA-VZUG-BubbleCount/IngotBubbles/sqlUpsertCommand",
					"Param.1": oViewData["ID"],
					"Param.2": DateFormat.getDateInstance({
							pattern: "yyyy-MM-ddTHH:mm:ss", UTC: true
						}).format(oViewData["InspectionDatetimeUTC"]),
					"Param.3": oViewData["Machine"],
					"Param.4": oViewData["ProductionRun"],
					"Param.5": oViewData["Particles"],
					"Param.6": oViewData["BubblesLT2mm"],
					"Param.7": oViewData["Bubbles2-4mm"],
					"Param.8": oViewData["Bubbles4-8mm"],
					"Param.9": oViewData["Bubbles8-12mm"],
					"Param.10": oViewData["BubblesGT12mm"],
					"Param.11": oViewData["SurfaceBubblesLT30mm"],
					"Param.12": oViewData["SurfaceBubblesGT30mm"],
					"Param.13": oViewData["Inspector"],
					"Param.14": oViewData["Remark"],
					"xsrfid": oModel.getSecurityToken(),
					"$format":"json"
				},			    
			    success: fnSuccess,
			    error: fnError
			};
			var sPath = "/QueryTemplate";
			oModel.create(sPath, null, mParameters);
			debugger;
		},
        loadViewData: function(){
        	var oRouteArgs = this.getRouteParameters().arguments;
        	//var oModel = new sap.ui.model.odata.v2.ODataModel();
        	var oModel = this.getCompModel();
        	var that = this;
			var fnSuccess = function(oData){
				debugger;
				
				var oRow = oData.results[0].Rowset.results[0].Row.results[0];
				oRow.CreatedDatetimeUTC = that.formatter.jsonDate(oRow.CreatedDatetimeUTC);
				oRow.ChangedDatetimeUTC = that.formatter.jsonDate(oRow.ChangedDatetimeUTC);
				oRow.InspectionDatetimeUTC = that.formatter.jsonDate(oRow.InspectionDatetimeUTC);
				that.getView().getModel("acquisition").setData(oRow);
				that.updateMachineSelect();
				//var sMessage = oData.results[0].Messages.results[0].Message;
				//sap.m.MessageToast.show(sMessage);
			};
			var fnError = function(oEvent){
				debugger;
			};
			var mParameters = {
			    headers: {},
				urlParameters: {
					"QueryTemplate": "HQS-HCA-VZUG-BubbleCount/IngotBubbles/sqlQueryRead",
					"FilterExpr": "[ID]=" + oRouteArgs.ID.toString(),
					"xsrfid": oModel.getSecurityToken(),
					"$format":"json"
				},			    
			    success: fnSuccess,
			    error: fnError
			};
			var sPath = "/QueryTemplate";
			//oModel.create(sPath, null, mParameters);
			oModel.read(sPath, mParameters);
			//oProductDetailPanel.bindElement({ path: sPath, model: "products" });
			// this.getView().bindElement({
			// 	path:"/",
			// 	model:"acquistion",
			// 	events: {
			// 		change : this._onBindingChange.bind(this),
			// 		dataRequested : function () {
			// 			//oViewModel.setProperty("/busy", true);
			// 			debugger;
			// 		},
			// 		dataReceived: function () {
			// 			//oViewModel.setProperty("/busy", false);
			// 			debugger;
			// 		}
			// 	}				
			// });
			// this.getModel().metadataLoaded().then( function() {
			// 		var sObjectPath = this.getModel().createKey("Objects", {
			// 			ObjectID :  sObjectId
			// 		});
			// 		this._bindView("/" + sObjectPath);
			// 	}.bind(this));			
			
			//this.getView().bindElement(sPath, mParameters);			
        },	
		_onBindingChange : function () {
			debugger;
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}

			var sPath = oElementBinding.getPath(),
				oResourceBundle = this.getResourceBundle(),
				oObject = oView.getModel().getObject(sPath),
				sObjectId = oObject.ObjectID,
				sObjectName = oObject.Name,
				oViewModel = this.getModel("detailView");

			this.getOwnerComponent().oListSelector.selectAListItem(sPath);

			oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage",
				oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
		},        
		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView : function (sObjectPath) {
			
			var sPath = "/Rowset(QueryTemplate=\'HQS-HCA-VZUG-BubbleCount%2FIngotBubbles%2FsqlQueryRead\',RowsetId=\'1\')/Row'";
			var mParameters = { custom: {DateColumn: 'InspectionDatetimeUTC', TimePeriod:'CurrentMonth'} };
			
			this.getView().bindElement({
				path : sPath,
				
				events: {
					change : this._onBindingChange.bind(this),
					dataRequested : function () {
						//oViewModel.setProperty("/busy", true);
						debugger;
					},
					dataReceived: function () {
						//oViewModel.setProperty("/busy", false);
						debugger;
					}
				}
			});        	
        },
        

		// getNewAcquisitionData: function(){
		// 	return {
		// 		"RowId": null,
		// 		"ID": null,
		// 		"Machine": "",
		// 		"ProductionRun": "",
		// 		"ChangedDatetimeUTC": new Date(),
		// 		"CreatedDatetimeUTC": new Date(),
		// 		"InspectionDatetimeUTC": new Date(),
		// 		"Particles": null,
		// 		"Bubbles2-4mm": null,
		// 		"Bubbles4-8mm": null,
		// 		"Bubbles8-12mm": null,
		// 		"BubblesGT12mm": null,
		// 		"BubblesLT2mm": null,
		// 		"SurfaceBubblesGT30mm": null,
		// 		"SurfaceBubblesLT30mm": null,
		// 		"Remark": "x",
		// 		"Inspector": ""
		// 	};			
		// },
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf BubbleCount.view.Acquisition
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf BubbleCount.view.Acquisition
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf BubbleCount.view.Acquisition
		 */
		//	onExit: function() {
		//
		//	}
		onMachineSelectChange: function(oEvent){
			var oItemsBindInfo = oEvent.getSource().getBindingInfo("items");
			var oSelectedItem = oEvent.getSource().getSelectedItem();
			var oMachineData = oSelectedItem.getBindingContext(oItemsBindInfo.model).getObject();
			var oViewModelData = this.getView().getModel("acquisition").getData();
			oViewModelData.Machine = oMachineData.value;

			// var oView = this.getView(),
			// 	oViewElementBinding = oView.getElementBinding();			
			// No data for the binding
			// if (!oViewElementBinding){
			// 	var oViewBoundContext = oViewElementBinding.getBoundContext();

			// }
			//this.getView().get oMachineData.value
			
		},
		onInspDatePickerChange: function(oEvent){
			var oViewModel = this.getView().getModel("acquisition");
			var sValue = oEvent.getSource().getValue();
			var oValueBinding = oEvent.getSource().getBinding("value");
			var dRawValue = oValueBinding.getRawValue();
			var dNewValue = oValueBinding.getType().parseValue(sValue,"string");
			//var oDateType = new sap.ui.model.type.DateTime({source: {pattern: "dd.MM.yyyy"}, pattern: "timestamp"});
			//var dValue = oDateType.parseValue(oValue);
            // var sDate = DateFormat.getDateInstance({
            //         pattern: sFormat
            //     }).format(oValue);
			
			
			
			debugger;
			//Copy date object into new variable
			//var dInspDate = new Date(oViewModel.getData().InspectionDatetimeUTC.getTime());
			dNewValue.setHours(dRawValue.getHours(),dRawValue.getMinutes(),dRawValue.getSeconds(),dRawValue.getMilliseconds());
			oViewModel.setProperty("/InspectionDatetimeUTC",dNewValue);
		},
		onInspTimeStepInputChange: function(oEvent){
			var oViewModel = this.getView().getModel("acquisition");
			var oViewModelData = this.getView().getModel("acquisition").getData();
			var iHours = oEvent.getSource().getValue();
			debugger;
			//var dInspDate = oViewModel.getData().InspectionDatetimeUTC;
			//Copy date object into new variable
			var dInspDate = new Date(oViewModel.getData().InspectionDatetimeUTC.getTime());
			dInspDate.setHours(iHours,0,0,0);
			//oViewModel.getData().InspectionDatetimeUTC = dInspDate;
			oViewModel.setProperty("/InspectionDatetimeUTC",dInspDate);
			
		},
		onParticlesStepInputChange: function(oEvent){
			
		},
		onBubblesLT2mmStepInputChange: function(oEvent){
			
		},
		onBubbles2_4mmStepInputChange: function(oEvent){
			
		},
		onBubbles4_8mmStepInputChange: function(oEvent){
			
		},
		onBubbles8_12mmStepInputChange: function(oEvent){
			
		},
		onBubblesGT12mmStepInputChange: function(oEvent){
			
		},
		onSurfaceBubblesLT30mmStepInputChange: function(oEvent){
			
		},
		onSurfaceBubblesGT30mmStepInputChange: function(oEvent){
			
		},
		onAcceptAction: function(oEvent){
			this.saveViewData();
		},
		onCancelAction: function(oEvent){
			debugger;
		},
		onRestartAction: function(oEvent){
			this.initViewData();
		},
		onShowOverview: function(oEvent){
			var oMachineObj = this.getSelectedMachineObject();
			this.getRouter().navTo("overview", {Plant:oMachineObj.plant, Workcenter:oMachineObj.workcenter});
		}
		//https://answers.sap.com/questions/318113/calling-post-method-from-ui5.html
		//https://sapui5.hana.ondemand.com/1.28.33/docs/guide/91f2a6ac6f4d1014b6dd926db0e91070.html
	});

});