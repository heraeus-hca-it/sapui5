sap.ui.define([
	"BubbleCount/controller/BaseController",
	"sap/ui/core/format/DateFormat"
], function(BaseController, DateFormat) {
	"use strict";

	return BaseController.extend("BubbleCount.controller.Overview", {
		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private IlluminatorOData>/Rowset(QueryTemplate=\'MyContent_f23440%2FMF%2FVPullTime%2FGetAggGeometryData\',RowsetId=\'1\')/Row
		 */
		_onRoutePatternMatched: function(oEvent) {
			//Store the original route parameters 
			this._routeParameters = oEvent.getParameters();			
			
			var that = this;
			var fnUpdate = function(){
				that.updateMachineSelect();
				that.onTableRefreshPress();
				//that.refreshBusyAndDelay();
			};			
			var aPromises = [
				this.getCompModel().metadataLoaded(), this.getCompModel("app").loaded()
			];
			Promise.all(aPromises).then(fnUpdate);
		},
		
		updateMachineSelect: function(){
			var oRouteArgs = this.getRouteParameters().arguments;
			var oMachineSelect = this.getView().byId("machineSelect");
			var oBindingInfo = oMachineSelect.getBindingInfo("items");
			var oSelectedItem = oMachineSelect.getItems().filter(function(item){
				var obj = item.getBindingContext(oBindingInfo.model).getObject();
				return obj.workcenter === oRouteArgs.Workcenter && obj.plant === oRouteArgs.Plant;
			},this)[0];
			if (oSelectedItem) {
				oMachineSelect.setSelectedItem(oSelectedItem);		
			}
		},
		
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */		
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf BubbleCount.view.Overview
		 */
		onInit: function() {
			this.getRouter().getRoute("overview").attachPatternMatched(this._onRoutePatternMatched, this);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf BubbleCount.view.Overview
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf BubbleCount.view.Overview
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf BubbleCount.view.Overview
		 */
		//	onExit: function() {
		//
		//	}
		onItemFormatError:function(oEvent){
			debugger;
		},
		onItemPress:function(oEvent){
			debugger;
		},
		onAddNewButtonPress:function(oEvent){
			var oMachineSelect = this.getView().byId("machineSelect");
			var oMachineSelectedItem = oMachineSelect.getSelectedItem();
			if (oMachineSelectedItem !== null){
				var sMachineModel = oMachineSelect.getBindingInfo("items").model;
				var oMachine = oMachineSelectedItem.getBindingContext(sMachineModel).getObject();
				this.getRouter().navTo("acquisition", {Plant:oMachine.plant,Workcenter:oMachine.workcenter});
			} else {
				
			}			
			
			debugger;
		},
		onEditButtonPress:function(oEvent){
			var oTable = this.getView().byId("overviewTable");
			var oItem = oEvent.getSource().getParent();
			var oBindingInfo = oTable.getBindingInfo("items");
			var oItemObject = oItem.getBindingContext(oBindingInfo.model).getObject();
			debugger;
			//this.getRouter().navTo("editobject", { ID: oItemObject.ID } );
			this.getRouter().navTo("acquisition",{ID: oItemObject.ID});
			
		},
		onDeleteButtonPress:function(oEvent){
			var oTable = this.getView().byId("overviewTable");
			var oItem = oEvent.getSource().getParent();
			var oBindingInfo = oTable.getBindingInfo("items");
			var oItemObject = oItem.getBindingContext(oBindingInfo.model).getObject();

			
			
			//https://sapui5.hana.ondemand.com/1.34.13/docs/guide/6c47b2b39db9404582994070ec3d57a2.html
			//var oModel = new sap.ui.model.odata.v2.ODataModel();
			var oModel = this.getCompModel();
			
			// var fnRefreshSecurityTokenSuccess = function(){
				
			// };
			// var fnRefreshSecurityTokenError = function(oError){
			// 	debugger;
			// };
			// oModel.refreshSecurityToken(fnRefreshSecurityTokenSuccess,fnRefreshSecurityTokenError,false);
			
			// var fnSuccess = function(oEvent){
			// 	debugger;
			// };
			// var fnError = function(oEvent){
			// 	debugger;
			// };
			
			// var mParameters = {
			// 	headers: {},
			// 	custom: {},
			// 	urlParameters: {
			// 		"xsrfid": "Fetch"
			// 	},
			// 	success: fnSuccess,
			// 	error: fnError
			// };
			// oModel.read("/QueryTemplate", mParameters);
			
			//oModel.callFunction("/QueryTemplate", { method:"GET", urlParameters:{"xsrfid":"Fetch"}, success:fnSuccess, error: fnError});
			
			// {
			// 	custom:{
			// 		"xsrfid":"Fetch"
			// 	}
			// 	, success: function(oEvent){
			// 		debugger;
			// 	}
			// 	, error: function(oEvent){
			// 		debugger;
			// 	}
			// });
			// /QueryTemplate?xsrfid=Fetch
			
			
			var fnSuccess = function(oData){
				var sMessage = oData.results[0].Messages.results[0].Message;
				sap.m.MessageToast.show(sMessage);
			};
			var fnError = function(oEvent){
				debugger;
			};
			var mParameters = {
			    headers: {},
				urlParameters: {
					"QueryTemplate": "HQS-HCA-VZUG-BubbleCount/IngotBubbles/sqlDeleteCommand",
					"Param.1": oItemObject.ID,
					"xsrfid": oModel.getSecurityToken()
				},			    
			    success: fnSuccess,
			    error: fnError
			};
			var sPath = "/QueryTemplate";
			oModel.create(sPath, null, mParameters);
		},
		onMachineSelectChange:function(oEvent){
			var oSelect = oEvent.getSource();
			var oBindingInfo = oSelect.getBindingInfo("items");
			var oMachine = oSelect.getSelectedItem().getBindingContext(oBindingInfo.model).getObject();
			this.getRouter().navTo("overview", {Plant:oMachine.plant,Workcenter:oMachine.workcenter});
		},
		onTableRefreshPress: function() {
			var oMachineSelect = this.getView().byId("machineSelect");
			
			var oMachineSelectedItem = oMachineSelect.getSelectedItem();
			
			var oTable = this.getView().byId("overviewTable");
			var oBindingInfo = oTable.getBindingInfo("items");
			if (!oBindingInfo.parameters) {
				oBindingInfo.parameters = {};
			}
			if (!oBindingInfo.parameters.custom) {
				oBindingInfo.parameters.custom = {};
			}
			var oCustomParams = oBindingInfo.parameters.custom;
			oCustomParams.DateColumn = "InspectionDatetimeUTC";
			oCustomParams.Dateformat = "yyyy-MM-dd'T'HH:mm:ss";
			var dEndDate = new Date();
			oCustomParams.EndDate = DateFormat.getDateInstance({
				pattern: "yyyy-MM-ddTHH:mm:ss", UTC: true
			}).format(dEndDate);
			var dStartDate = new Date(dEndDate.getTime());
			dStartDate.setDate(dEndDate.getDate() - 1);
			oCustomParams.StartDate = DateFormat.getDateInstance({
				pattern: "yyyy-MM-ddTHH:mm:ss", UTC: true
			}).format(dStartDate);				
			//oCustomParams.StartDate = "2020-07-17T14:12:00";
			//oCustomParams.EndDate = "2022-07-17T14:14:00";
			if (oMachineSelectedItem !== null){
				var sMachineModel = oMachineSelect.getBindingInfo("items").model;
				var oMachine = oMachineSelectedItem.getBindingContext(sMachineModel).getObject();
				oCustomParams.FilterExpr = "[Machine]='" + oMachine.value + "'";
			} else {
				oCustomParams.FilterExpr = "[Machine]=''";
			}
			oTable.bindItems(oBindingInfo);
			oTable.getBinding("items").refresh();
			oTable.getBinding("items").resume(); 
			
		}
	});
});