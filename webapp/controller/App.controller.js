sap.ui.define([
		"BubbleCount/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function (BaseController, JSONModel) {
		"use strict";
		return BaseController.extend("BubbleCount.controller.App", {
			onInit : function () {
				// var oListSelector = this.getOwnerComponent().oListSelector;
				
				// // Makes sure that master view is hidden in split app
				// // after a new list entry has been selected.
				// oListSelector.attachListSelectionChange(function () {
				// 	this.byId("idAppControl").hideMaster();
				// }, this);

        		// always use absolute paths relative to our own component
        		// (relative paths will fail if running in the Fiori Launchpad)
        		//var rootPath = jQuery.sap.getModulePath("BubbleCount");
				
				//var sImagePath =  sap.ui.require.toUrl("BubbleCount/images/Heraeus_Conamic_RGB_Web_SafeZone_TranspBG.png");
				var sImagePath =  sap.ui.require.toUrl("BubbleCount/images/Heraeus_Logo_RGB_Web_SafeZone_TranspBG.png");
				this.byId("shellBar").setHomeIcon(sImagePath);
				
				//sap.ui.Device.resize.attachHandler(this.onDeviceResize, oListener?)
				
				// apply content density mode to root view
				this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
				
// function sizeChanged(mParams) {
//     switch(mParams.name) {
//         case "Phone":
//             // Do what is needed for a little screen
//             break;
//         case "Tablet":
//             // Do what is needed for a medium sized screen
//             break;
//         case "Desktop":
//             // Do what is needed for a large screen
//     }
// }

// // Register an event handler to changes of the screen size
// sap.ui.Device.media.attachHandler(sizeChanged, null, sap.ui.Device.media.RANGESETS.SAP_STANDARD);
// // Do some initialization work based on the current size
// sizeChanged(sap.ui.Device.media.getCurrentRange(sap.ui.Device.media.RANGESETS.SAP_STANDARD));
				
			},

			
			onMenuButtonPress : function() {
				var toolPage = this.byId("toolPage");
				toolPage.setSideExpanded(!toolPage.getSideExpanded());
			},			
			onItemSelect: function(oEvent){
				
			},
			onListItemPress : function(oEvent) {
				var sTarget = oEvent.getSource().getCustomData()[0].getValue();
				this.getRouter().navTo(sTarget, {});
			},
			onAppStartItemPress : function(oEvent) {
				this.getRouter().navTo("appstart", {});
			},
			onAcquistionItemPress : function(oEvent) {
				//this.getRouter().navTo("acquisition", {Plant:"", Workcenter:""});
				this.getRouter().navTo("acquisition");
			},
			onOverviewItemPress : function(oEvent) {
				//this.getRouter().navTo("overview", {Plant:"", Workcenter:""});
				this.getRouter().navTo("overview");
			},
			onSplunkReportsPress : function(oEvent) {
				var sUrl = "https://splunk-iot.heraeus.com/de-DE/app/her_app_hca_basematerials/search";
				sap.m.URLHelper.redirect(sUrl, true);
			}
			
		});
	}
);