sap.ui.define([
		"sap/m/MessageBox",
		"sap/ui/model/json/JSONModel",
		"sap/ui/model/xml/XMLModel",
		"sap/ui/Device",
		"sap/ui/core/message/Message",
		"sap/ui/core/MessageType",
		"BubbleCount/model/formatter"
	], function (MessageBox, JSONModel, XMLModel, Device, Message, MessageType, formatter) {
		"use strict";

		var models = {
			attrToObj : function(oNode){
        		var attributes = {}; 
        		if( this.length ) {
            		$.each( oNode.attributes, function( index, attr ) {
                		attributes[ attr.name ] = attr.value;
            		} ); 
        		}
        		return attributes;
			},
			createDeviceModel : function () {
				var oModel = new JSONModel(Device);
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			},
			createFLPModel : function () {
				var fnGetUser = jQuery.sap.getObject("sap.ushell.Container.getUser"),
					bIsShareInJamActive = fnGetUser ? fnGetUser().isJamActive() : false,
					oModel = new JSONModel({
						isShareInJamActive: bIsShareInJamActive
					});
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			}
		};
		return models;
	}
);