sap.ui.define([
	"BubbleCount/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("BubbleCount.controller.AppStart", {
	
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf BubbleCount.view.AppStart
		 */
		onInit: function() {
			var that = this,
				oAppModel = this.getCompModel("app"),
				oRouter = this.getRouter();
				
			// Store original busy indicator delay, so it can be restored later on
			//this._originalBusyDelay = this.getView().getBusyIndicatorDelay();
			// var oMessageManager  = sap.ui.getCore().getMessageManager();
			// oMessageManager.registerMessageProcessor(oAppModel);
			
			//Inititialze controls
			
			//var oBinding = new sap.ui.model.Binding(oAppModel, "/oper", oAppModel.getContext("/oper"));
			// var oBinding = new sap.ui.model.Binding(oAppModel, "/oper");
			// oBinding.attachChange(function(oEvent) {
			// 	var oModel = oEvent.getSource().getModel(),
			// 		sPath = oEvent.getSource().getPath(),
			// 		oValue = oModel.getObject(sPath);
			// 	debugger;
   // 			//or anything else
			// });			
			//https://sapui5.hana.ondemand.com/#/topic/8956f0a223284d729900ebad4ca88356
			// var oBinding = oAppModel.bindProperty("/");
			// oBinding.attachChange(function(oEvent) {
			//  	var oBinding = oEvent.getSource(),
			//  		oValue = oBinding.getValue();
			//  	debugger;
			//  }, this);			
			//do not use NaN
			//oAppModel.bindProperty("/oper/operno").attachChange(this._onOperPropertyChange, this);			
					
		},
		onMachineTilePress: function(oEvent){
			var oTile = oEvent.getSource();
			//var oBinding = oTile.getParent().getBinding("content");
			var oBindingInfo = oTile.getParent().getBindingInfo("content");
			//var sContext = oBinding.getContext();
			
			
			
			//var oMachine = oTile.getBindingContext("app").getObject();
			
			var oMachine = oTile.getBindingContext(oBindingInfo.model).getObject();
			var sPlant = oMachine.plant;
			var sWorkcenter = oMachine.workcenter;
			this.getRouter().navTo("acquisition", {Plant:sPlant, Workcenter:sWorkcenter});
			debugger;
		}
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf BubbleCount.view.AppStart
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf BubbleCount.view.AppStart
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf BubbleCount.view.AppStart
		 */
		//	onExit: function() {
		//
		//	}

	});

});